<?php include_once('inc/header.php') ?>

<?php

$allpostQuery = "SELECT * FROM post WHERE status= 'Publish'";
$allResults = mysqli_query($connection, $allpostQuery);

?>

<section id="main-content">
  <div class="full-width mt-40">
    <div class="fix-width">
      <div class="row">
        <?php while ($results = mysqli_fetch_assoc($allResults)) : ?>
          <div class="card">
            <div class="media">
              <img src="./img/classroom/1.jpg" alt="" />
              <div class="user-info">
                <h3><?= $results['title'] ?></h3>
                <small>1h ago</small>
              </div>
            </div>
            <!-- <div class="post-img">
            <img src="./img/classroom/1.jpg" alt="" />
          </div> -->
            <div class="post-content">
              <?= $results['description'] ?>
            </div>
            <a href="#">Read more</a>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>

<?php include_once('inc/footer.php') ?>