<?php include_once('inc/header.php') ?>

<?php

if (isset($_POST) && !empty($_POST)) {
  $title = $_POST['title'];
  $description = $_POST['description'];
  $status = $_POST['status'];

  $sql = "INSERT INTO `post`(`title`, `description`, `status`) values('$title', '$description', '$status')";
  $insert = mysqli_query($connection, $sql);
  if ($insert) {
    header('Location: ./');
  } else {
    echo "something wrong";
  }
}

?>

<section id="main-content">
  <div class="full-width mt-40">
    <div class="fix-width">
      <div class="flx-box">
        <div class="card-create-post">
          <form action="" method="POST">
            <div class="input-group">
              <label for="title">Title</label>
              <input class="input" placeholder="Enter your title" type="text" name="title" id="title" />
            </div>
            <div class="input-group">
              <label for="status">Status</label>
              <select class="input" name="status" id="status">
                <option value="">Select One</option>
                <option value="Publish">Publish</option>
                <option value="Draft">Draft</option>
              </select>
            </div>
            <div class="input-group">
              <label for="post">Post</label>
              <textarea placeholder="Enter your titile" class="input" name="description" id="post" cols="30" rows="10"></textarea>
            </div>
            <button class="submit-btn" type="submit">Post</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include_once('inc/footer.php') ?>