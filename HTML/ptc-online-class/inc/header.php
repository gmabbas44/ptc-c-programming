<?php include_once('inc/library.php') ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>PTC Online class</title>
  <link rel="stylesheet" href="./style.css" />
</head>

<body>
  <header id="header">
    <div class="full-width">
      <div class="fix-width">
        <div class="flx">
          <div class="logo">
            <img src="./img/logo/logo.png" alt="" />
          </div>
          <div class="manu">
            <nav>
              <span><a href="./">Home</a></span>
              <span><a href="./all-post.php">All Post</a></span>
              <span><a href="./add-post.php">Add Post</a></span>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>