-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 25, 2021 at 04:01 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptc`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `is_deleted` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `title`, `description`, `status`, `is_deleted`, `created_at`, `modified_at`) VALUES
(1, 'Lorem Ipsum is simply ', 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo consectetur totam atque, recusandae corrupti dicta eum vel cumque eligendi tempora vitae possimus, repellendus optio eveniet reprehenderit consequatur suscipit vero soluta! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo consectetur totam atque,', 'Publish', 0, '2021-06-25 15:04:00', '2021-06-25 15:04:00'),
(2, 'Lorem Ipsum is simply dummy text of the printing', 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo consectetur totam atque, recusandae corrupti dicta eum vel cumque eligendi tempora vitae possimus, repellendus optio eveniet reprehenderit consequatur suscipit vero soluta! Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nemo consectetur totam atque,', 'Publish', 0, '2021-06-25 15:04:02', '2021-06-25 15:04:02'),
(3, 'This is PTC Online class.', 'our student is great, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', 'Publish', 0, '2021-06-25 15:19:57', '2021-06-25 15:19:57'),
(4, 'This is PTC Online class.', 'our student is great, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,', 'Publish', 0, '2021-06-25 15:20:00', '2021-06-25 15:20:00'),
(5, 'gm abbas uddin', 'cimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing L', 'Draft', 0, '2021-06-25 15:49:22', '2021-06-25 15:49:22'),
(6, 'PTC Students', 'cimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. ', 'Publish', 0, '2021-06-25 15:50:13', '2021-06-25 15:50:13'),
(7, 'PTC Students', 'cimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing L', 'Draft', 0, '2021-06-25 15:56:14', '2021-06-25 15:56:14'),
(8, 'Lorem Ipsum', 'cimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing L', 'Publish', 0, '2021-06-25 15:58:37', '2021-06-25 15:58:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
