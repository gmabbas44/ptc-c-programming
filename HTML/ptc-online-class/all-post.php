<?php include_once('inc/header.php') ?>

<section id="main-content">
  <div class="full-width mt-40">
    <div class="fix-width">
      <div class="card-table">
        <table class="table">
          <thead>
            <th>id</th>
            <th>Title</th>
            <th>Descitption</th>
            <th>Status</th>
            <th>Action</th>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
              </td>
              <td>
                Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                Laborum vero asperiores debitis corrupti ipsa, iure
                molestias cumque et commodi ratione, laboriosam officiis
                harum adipisci laudantium ab dolores dolor fugit quasi!
              </td>
              <td>Draft</td>
              <td>
                <div class="flx">
                  <a href="#" class="btn" title="Draft">
                    <img src="./iconscout-font/arrow-circle-down.svg" alt="" />
                  </a>
                  <a href="#" class="btn" title="Publish">
                    <img src="./iconscout-font/arrow-circle-up.svg" alt="" />
                  </a>
                  <a href="#" class="btn" title="Delete">
                    <img src="./iconscout-font/trash-alt.svg" alt="" />
                  </a>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

<?php include_once('inc/footer.php') ?>