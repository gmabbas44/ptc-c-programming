#include <stdio.h>

void main()
{
    float r, PI, area;
    PI = 3.1416;
    printf("Enter Circle Radius : ");
    scanf("%f", &r);
    area = PI * r * r;
    printf("Your Circle area is : %.2f", area);

}
