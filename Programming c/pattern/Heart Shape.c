#include <stdio.h>

void main()
{
    int col, row;

    for (row = 0; row <= 6; row++)
    {
        for (col = 0; col <= 5; col++)
        {
            if(row == 0 && col%3 == 0 || row == 1 && col%3 == 0 || row - col == 2 || row + col == 8){
                printf("* ");
            }
            printf(" ");
        }
        printf("\n");
    }
}
