#include <stdio.h>

void main()
{
    float x, mother_age, father_age;

    printf("Enter son's age\t: ");
    scanf("%f", &x);

    mother_age = x * 3;
    father_age = mother_age + 5;

    printf("Mother age\t: %f\n", mother_age);
    printf("Father age\t: %f\n", father_age);
}
