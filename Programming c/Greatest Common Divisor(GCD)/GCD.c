#include <stdio.h>

void main()
{
    int num1 = 60, num2 = 24;
    int n1 = num1, n2 = num2, remn;

    while(n2 != 0)
    {
        remn = n1 % n2 ;
        n1 = n2;
        n2 = remn;
    }
    printf("%d",n1);
}
