#include <stdio.h>

void main()
{
    int num1, num2, num3, sum, avg;

    num1 = 20;
    num2 = 10;
    num3 = 30;

    sum = num1 + num2 + num3;
    avg = sum / 3;

    /*
    printf("Your first number is : %d\n", num1);
    printf("Your second number is : %d\n", num2);
    printf("Your third number is : %d\n", num3);
    */
    printf("The sum results of : %d\n",sum);
    printf("The sum average results of : %d",avg);
}
