#include <stdio.h>
#include <math.h>

void main()
{
    float a, b, area;
    printf("Enter Equal arm length\t: ");
    scanf("%f",&a);
    printf("Enter Triangle Base\t: ");
    scanf("%f", &b);

    area = b / 4 * sqrt(4 * pow(a,2) - pow(b,2));
    // area = b / 4 * sqrt(4 * a * a - b * b);

    printf("Your Results is\t\t: %.2f\n",area);
}
