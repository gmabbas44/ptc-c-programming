#include <stdio.h>

void main()
{
    float num1, num2, sum, multi;

    printf("Enter First number\t: ");
    scanf("%f",&num1);
    printf("Enter Second number\t: ");
    scanf("%f", &num2);

    sum = num1 + num2;
    multi = sum + num1;
    printf("The sum result is\t: %f\n",sum);
    printf("The multi result is\t: %f\n",multi);
}
