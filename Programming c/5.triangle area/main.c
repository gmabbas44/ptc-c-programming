#include <stdio.h>

void main()
{
    float height, base, area;

    printf("Enter Triangle Height : ");
    scanf("%f", &height);
    printf("Enter Triangle Base : ");
    scanf("%f", &base);
    area = .5 * height * base;
    printf("The Triangle Area is : %.2f", area);
}
