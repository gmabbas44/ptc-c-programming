#include <stdio.h>

//question no-19/20 : 1+2+3+4+..........+n = ?

void main()
{
    int i=1, n, sum = 0;
    printf("Enter your number : ");
    scanf("%d", &n);

    while(i <= n){
        sum = sum + i;
        i++;
    }
    printf("1+2+3+4+.......+%d = %d",n,sum);
}

