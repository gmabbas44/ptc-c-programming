#include <stdio.h>

//question no-19/20 : 1+2+3+4+..........+n = ?

void main()
{
    int i, n, sum = 0;
    printf("Enter your number : ");
    scanf("%d", &n);

    for(i = 1; i <= n; i++)
    {
        sum = sum + i;
    }
    printf("1+2+3+4+.......+%d = %d",n,sum);
}

